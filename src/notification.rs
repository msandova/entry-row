use adw::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};
use gtk::{prelude::*, CompositeTemplate};
use once_cell::sync::Lazy;

mod imp {
    use std::cell::RefCell;

    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/entry-row/ui/notification.ui")]
    pub struct Notification {
        pub title: RefCell<Option<String>>,
        pub action_name: RefCell<Option<String>>,
        pub button_label: RefCell<Option<String>>,
        #[template_child]
        box_: TemplateChild<gtk::Box>,
        #[template_child]
        pub action_bin: TemplateChild<adw::Bin>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Notification {
        const NAME: &'static str = "Notification";
        type Type = super::Notification;
        type ParentType = gtk::Widget;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
            klass.install_action("close", None, move |obj, _, _| {
                obj.unparent();
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Notification {
        fn dispose(&self, _obj: &Self::Type) {
            self.box_.unparent();
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                vec![
                    glib::ParamSpec::new_string(
                        "title",
                        "title",
                        "title",
                        None,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT,
                    ),
                    glib::ParamSpec::new_string(
                        "action-name",
                        "action-name",
                        "action-name",
                        None,
                        glib::ParamFlags::EXPLICIT_NOTIFY
                            | glib::ParamFlags::READWRITE
                            | glib::ParamFlags::CONSTRUCT_ONLY,
                    ),
                    glib::ParamSpec::new_string(
                        "button-label",
                        "button-label",
                        "button-label",
                        None,
                        glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "title" => self.title.borrow().to_value(),
                "action-name" => self.action_name.borrow().to_value(),
                "button-label" => self.button_label.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "title" => {
                    self.title.replace(value.get().unwrap());
                    ()
                }
                "button-label" => {
                    self.button_label.replace(value.get().unwrap());
                    ()
                }
                "action-name" => obj.set_action_name(value.get().unwrap()),
                _ => unimplemented!(),
            };
        }
    }
    impl WidgetImpl for Notification {}
}

glib::wrapper! {
    pub struct Notification(ObjectSubclass<imp::Notification>)
        @extends gtk::Widget;
}

impl Default for Notification {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl Notification {
    pub fn set_action_name(&self, action_name: &str) {
        let self_ = imp::Notification::from_instance(self);

        let button = gtk::Button::new();
        button.set_action_name(Some(action_name));
        self_.action_bin.set_child(Some(&button));
        self.bind_property("button-label", &button, "label")
            .flags(glib::BindingFlags::SYNC_CREATE)
            .build();

        self_.action_name.replace(Some(action_name.to_string()));
        self.notify("action-name");
    }

    fn from_gnotification(&self, notification: gio::Notification) -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

// TODO needs action-target and clicked signal
