use gtk::gdk;
use gtk::glib;
use gtk::glib::clone;
use gtk::subclass::prelude::*;
use gtk::{prelude::*, CompositeTemplate};

use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};

mod imp {
    use super::*;
    use glib::object::{Interface, InterfaceRef};

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/entry-row/ui/scrollable_view.ui")]
    pub struct ScrollableView {
        #[template_child]
        pub prefixes: TemplateChild<gtk::Box>,
        #[template_child]
        pub box_: TemplateChild<gtk::Box>,
        #[template_child]
        pub list_view: TemplateChild<gtk::ListView>,

        pub hscroll_policy: Cell<gtk::ScrollablePolicy>,
        pub vscroll_policy: Cell<gtk::ScrollablePolicy>,
        pub vadjustment: RefCell<Option<gtk::Adjustment>>,
        pub vadjustment_signal: RefCell<Option<glib::SignalHandlerId>>,
        pub hadjustment: RefCell<Option<gtk::Adjustment>>,
        pub hadjustment_signal: RefCell<Option<glib::SignalHandlerId>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ScrollableView {
        const NAME: &'static str = "ScrollableView";
        type Type = super::ScrollableView;
        type ParentType = gtk::Widget;
        type Interfaces = (gtk::Buildable, gtk::Scrollable);

        fn new() -> Self {
            Self {
                prefixes: TemplateChild::default(),
                box_: TemplateChild::default(),
                list_view: TemplateChild::default(),

                hadjustment: RefCell::new(None),
                hadjustment_signal: RefCell::new(None),
                vadjustment: RefCell::new(None),
                vadjustment_signal: RefCell::new(None),

                hscroll_policy: Cell::new(gtk::ScrollablePolicy::Minimum),
                vscroll_policy: Cell::new(gtk::ScrollablePolicy::Minimum),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
            klass.set_layout_manager_type::<gtk::BinLayout>();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ScrollableView {
        fn dispose(&self, obj: &Self::Type) {
            self.box_.unparent();
        }

        fn constructed(&self, obj: &Self::Type) {
            self.parent_constructed(obj);

            let list = gtk::StringList::new(&[]);
            let model = gtk::NoSelection::new(Some(&list));
            for i in 0..500 {
                list.append(&i.to_string());
            }

            let factory = gtk::SignalListItemFactory::new();

            factory.connect_setup(|_, item| {
                let label = gtk::Label::new(None);
                item.set_child(Some(&label));
            });

            factory.connect_bind(|_, item| {
                let child = item.child().unwrap();
                let label = child.downcast_ref::<gtk::Label>().unwrap();

                let item = item
                    .item()
                    .unwrap()
                    .downcast::<gtk::StringObject>()
                    .unwrap();
                label.set_label(&item.string());
            });

            self.list_view.set_model(Some(&model));
            self.list_view.set_factory(Some(&factory));

            obj.set_overflow(gtk::Overflow::Hidden);
            // Controllers

            // let gesture_drag = gtk::GestureDrag::new();
            // gesture_drag.set_exclusive(true);
            // gesture_drag.connect_drag_begin(clone!(@weak obj => move |_, start_x, start_y| {
            //     let self_ = imp::DrawingArea::from_instance(&obj);
            //     let image_coords = obj.to_image_coords((start_x, start_y));
            //     self_.start_position.replace(Some(image_coords));
            // }));
            // gesture_drag.connect_drag_update(clone!(@weak obj => move |gesture, offset_x, offset_y| {
            //     let self_ = imp::DrawingArea::from_instance(&obj);
            //     if let Some(start_point) = gesture.start_point() {
            //         let (start_x, start_y) = start_point;
            //         let image_coords = obj.to_image_coords((start_x + offset_x, start_y + offset_y));
            //         self_.current_position.replace(Some(image_coords));
            //         obj.queue_draw();
            //     }
            // }));
            // gesture_drag.connect_drag_end(clone!(@weak obj => move |gesture, offset_x, offset_y| {
            //     let self_ = imp::DrawingArea::from_instance(&obj);
            //     if let Some(start_point) = gesture.start_point() {
            //         let (start_x, start_y) = start_point;
            //         let image_coords = obj.to_image_coords((start_x + offset_x, start_y + offset_y));
            //         self_.current_position.replace(Some(image_coords));
            //         obj.apply_filter();
            //     }
            // }));
            // gesture_drag.connect_cancel(clone!(@weak obj => move |_, _| {
            //     let self_ = imp::DrawingArea::from_instance(&obj);
            //     self_.start_position.replace(None);
            //     self_.current_position.replace(None);
            // }));
            // obj.add_controller(&gesture_drag);

            // let motion_controller = gtk::EventControllerMotion::new();
            // motion_controller.connect_enter(clone!(@weak obj => move |_, x, y| {
            //     let self_ = imp::DrawingArea::from_instance(&obj);
            //     self_.pointer_position.replace(Some((x, y)));
            // }));
            // motion_controller.connect_motion(clone!(@weak obj => move |_, x, y| {
            //     let self_ = imp::DrawingArea::from_instance(&obj);
            //     self_.pointer_position.replace(Some((x, y)));
            // }));
            // motion_controller.connect_leave(clone!(@weak obj => move |_| {
            //     let self_ = imp::DrawingArea::from_instance(&obj);
            //     self_.pointer_position.replace(None);
            // }));
            // obj.add_controller(&motion_controller);
        }

        fn properties() -> &'static [glib::ParamSpec] {
            static PROPERTIES: Lazy<Vec<glib::ParamSpec>> = Lazy::new(|| {
                let interface: InterfaceRef<gtk::Scrollable> =
                    Interface::from_type(gtk::Scrollable::static_type()).unwrap();
                vec![
                    // Scrollable properties
                    glib::ParamSpec::new_override(
                        "hscroll-policy",
                        &interface.find_property("hscroll-policy").unwrap(),
                    ),
                    glib::ParamSpec::new_override(
                        "vscroll-policy",
                        &interface.find_property("vscroll-policy").unwrap(),
                    ),
                    glib::ParamSpec::new_override(
                        "hadjustment",
                        &interface.find_property("hadjustment").unwrap(),
                    ),
                    glib::ParamSpec::new_override(
                        "vadjustment",
                        &interface.find_property("vadjustment").unwrap(),
                    ),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            match pspec.name() {
                "hadjustment" => self.hadjustment.borrow().to_value(),
                "vadjustment" => self.vadjustment.borrow().to_value(),
                "hscroll-policy" => self.hscroll_policy.get().to_value(),
                "vscroll-policy" => self.vscroll_policy.get().to_value(),
                _ => unimplemented!(),
            }
        }

        fn set_property(
            &self,
            obj: &Self::Type,
            _id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            match pspec.name() {
                "hadjustment" => {
                    let hadjustment = value.get().unwrap();
                    obj.set_hadjustment(hadjustment);
                }
                "hscroll-policy" => {
                    let hscroll_policy = value.get().unwrap();
                    self.hscroll_policy.replace(hscroll_policy);
                }
                "vadjustment" => {
                    let vadjustment = value.get().unwrap();
                    obj.set_vadjustment(vadjustment);
                }
                "vscroll-policy" => {
                    let vscroll_policy = value.get().unwrap();
                    self.vscroll_policy.replace(vscroll_policy);
                }
                _ => unimplemented!(),
            }
        }
    }
    impl WidgetImpl for ScrollableView {
        fn size_allocate(&self, widget: &Self::Type, width: i32, height: i32, baseline: i32) {
            widget.set_adjustment_values();

            let x = -self.hadjustment.borrow().as_ref().unwrap().value() as i32;
            let y = -self.vadjustment.borrow().as_ref().unwrap().value() as i32;

            let allocation = gtk::Allocation {
                x,
                y,
                width: self.hadjustment.borrow().as_ref().unwrap().upper() as i32,
                height: self.vadjustment.borrow().as_ref().unwrap().upper() as i32,
            };

            self.box_.size_allocate(&allocation, -1)
        }
    }
    impl ScrollableImpl for ScrollableView {}
    impl BuildableImpl for ScrollableView {
        fn add_child(
            &self,
            buildable: &Self::Type,
            builder: &gtk::Builder,
            child: &glib::Object,
            type_: Option<&str>,
        ) {
            if buildable.first_child().is_none() {
                self.parent_add_child(buildable, builder, child, type_);
            } else if let Some(child_type) = type_ {
                if child_type == "prefix" {
                    buildable.add_prefix(child.downcast_ref::<gtk::Widget>().unwrap());
                }
            };
        }
    }
}

glib::wrapper! {
    pub struct ScrollableView(ObjectSubclass<imp::ScrollableView>)
        @extends gtk::Widget, @implements gtk::Buildable;
}

impl Default for ScrollableView {
    fn default() -> Self {
        glib::Object::new(&[]).unwrap()
    }
}

impl ScrollableView {
    pub fn add_prefix<T: IsA<gtk::Widget>>(&self, widget: &T) {
        let self_ = imp::ScrollableView::from_instance(self);
        self_.prefixes.append(widget);
        self_.prefixes.show();
    }

    fn set_hadjustment(&self, adj: Option<gtk::Adjustment>) {
        let self_ = imp::ScrollableView::from_instance(self);
        self_.hadjustment.replace(adj);
    }

    fn set_vadjustment(&self, adj: Option<gtk::Adjustment>) {
        let self_ = imp::ScrollableView::from_instance(self);
        self_.vadjustment.replace(adj);
    }

    fn set_adjustment_values(&self) {
        let self_ = imp::ScrollableView::from_instance(self);

        let view_width = self.width();
        let view_height = self.height();

        let viewport_size = view_width;
        let other_viewport_size = view_height;
        let scroll_policy = self_.hscroll_policy.get();
        let other_scroll_policy = self_.vscroll_policy.get();

        //       adjustment = viewport->vadjustment;
        //       other_orientation = GTK_ORIENTATION_HORIZONTAL;
        //       viewport_size = view_height;
        //       other_viewport_size = view_width;
        //       scroll_policy = viewport->vscroll_policy;
        //       other_scroll_policy = viewport->hscroll_policy;
    }
}

//   if (viewport->child && gtk_widget_get_visible (viewport->child))
//     {
//       int min_size, nat_size;
//       int scroll_size;

//       if (other_scroll_policy == GTK_SCROLL_MINIMUM)
//         gtk_widget_measure (viewport->child, other_orientation, -1,
//                             &scroll_size, NULL, NULL, NULL);
//       else
//         gtk_widget_measure (viewport->child, other_orientation, -1,
//                             NULL, &scroll_size, NULL, NULL);

//       gtk_widget_measure (viewport->child, orientation,
//                           MAX (other_viewport_size, scroll_size),
//                           &min_size, &nat_size, NULL, NULL);

//       if (scroll_policy == GTK_SCROLL_MINIMUM)
//         upper = MAX (min_size, viewport_size);
//       else
//         upper = MAX (nat_size, viewport_size);

//     }
//   else
//     {
//       upper = viewport_size;
//     }

//   value = gtk_adjustment_get_value (adjustment);

//   /* We clamp to the left in RTL mode */
//   if (orientation == GTK_ORIENTATION_HORIZONTAL &&
//       _gtk_widget_get_direction (GTK_WIDGET (viewport)) == GTK_TEXT_DIR_RTL)
//     {
//       double dist = gtk_adjustment_get_upper (adjustment)
//                      - value
//                      - gtk_adjustment_get_page_size (adjustment);
//       value = upper - dist - viewport_size;
//     }

//   gtk_adjustment_configure (adjustment,
//                             value,
//                             0,
//                             upper,
//                             viewport_size * 0.1,
//                             viewport_size * 0.9,
//                             viewport_size);
// }
