use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::object::IsA;
use gtk::glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    use glib::ParamSpec;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/entry-row/ui/entry_row.ui")]
    pub struct EntryRow {
        #[template_child]
        pub text: TemplateChild<gtk::Text>,
        #[template_child]
        pub edit_icon: TemplateChild<gtk::Image>,
        #[template_child]
        pub title_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub subtitle_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub subtitle_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub suffixes: TemplateChild<gtk::Box>,
        #[template_child]
        pub prefixes: TemplateChild<gtk::Box>,
        #[template_child]
        pub header: TemplateChild<gtk::Box>,

        pub subtitle: RefCell<Option<String>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EntryRow {
        const NAME: &'static str = "EntryRow";
        type Type = super::EntryRow;
        type ParentType = adw::PreferencesRow;
        type Interfaces = (gtk::Editable, gtk::Buildable);

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EntryRow {
        fn constructed(&self, editable: &Self::Type) {
            editable.init_delegate();

            editable.connect_parent_notify(move |editable| {
                if let Some(parent) = editable.parent() {
                    if let Ok(listbox) = parent.downcast::<gtk::ListBox>() {
                        listbox.connect_row_activated(
                            clone!(@weak editable => move |_listbox, row| {
                                if editable.upcast_ref::<gtk::ListBoxRow>() == row {
                                    editable.set_editable(true);
                                } else {
                                    editable.set_editable(false);
                                };
                            }),
                        );
                    };
                };
            });

            self.text
                .connect_activate(clone!(@weak editable => move |text| {
                    text.set_editable(false);
                }));

            // We need to set the label as visible when
            // the text is set via `set_text`.
            editable.connect_changed(move |editable| {
                let self_ = imp::EntryRow::from_instance(&editable);
                self_.subtitle_stack.set_visible(true);

                editable.set_style();
            });

            editable
                .bind_property("title", &self.title_label.get(), "visible")
                .transform_to(|_, value| {
                    let value: Option<String> = value.get().ok();
                    value.map(|title| (!title.is_empty()).to_value())
                })
                .build();

            editable.connect_editable_notify(move |editable| {
                let is_editable = editable.is_editable();
                let self_ = imp::EntryRow::from_instance(&editable);
                if is_editable {
                    self_.subtitle_stack.set_visible(true);
                    self_.text.grab_focus();
                    editable.set_position(-1);
                }
                editable.set_style();
            });
            self.parent_constructed(editable);
        }
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| vec![]);
            PROPERTIES.as_ref()
        }

        fn set_property(
            &self,
            editable: &Self::Type,
            id: usize,
            value: &glib::Value,
            pspec: &glib::ParamSpec,
        ) {
            if !self.delegate_set_property(editable, id, value, pspec) {
                match pspec.name() {
                    _ => unimplemented!(),
                }
            }
        }

        fn property(
            &self,
            editable: &Self::Type,
            id: usize,
            pspec: &glib::ParamSpec,
        ) -> glib::Value {
            if let Some(value) = self.delegate_get_property(editable, id, pspec) {
                value
            } else {
                match pspec.name() {
                    _ => unimplemented!(),
                }
            }
        }
        fn dispose(&self, editable: &Self::Type) {
            editable.finish_delegate();
            self.text.unparent(); // Is this needed? There is no Gtk.Widget
            while let Some(child) = editable.first_child() {
                child.unparent(); // dito.
            }
        }
    }

    impl WidgetImpl for EntryRow {}
    impl ListBoxRowImpl for EntryRow {}
    impl PreferencesRowImpl for EntryRow {}
    impl EditableImpl for EntryRow {
        fn delegate(&self, _editable: &Self::Type) -> Option<gtk::Editable> {
            Some(self.text.clone().upcast())
        }
    }
    impl BuildableImpl for EntryRow {
        fn add_child(
            &self,
            buildable: &Self::Type,
            builder: &gtk::Builder,
            child: &glib::Object,
            type_: Option<&str>,
        ) {
            if buildable.first_child().is_none() {
                self.parent_add_child(buildable, builder, child, type_);
            } else if let Some(child_type) = type_ {
                if child_type == "prefix" {
                    buildable.add_prefix(child.downcast_ref::<gtk::Widget>().unwrap());
                } else if child_type == "suffix" {
                    buildable.add_suffix(child.downcast_ref::<gtk::Widget>().unwrap());
                }
            } else {
                buildable.add_suffix(child.downcast_ref::<gtk::Widget>().unwrap());
            };
        }
    }
}

glib::wrapper! {
    pub struct EntryRow(ObjectSubclass<imp::EntryRow>) @extends gtk::Widget, adw::PreferencesRow, gtk::ListBoxRow, @implements gtk::Editable, gtk::Buildable;
}

impl EntryRow {
    pub fn new() -> Self {
        glib::Object::new(&[]).unwrap()
    }

    pub fn add_suffix<T: IsA<gtk::Widget>>(&self, widget: &T) {
        let self_ = imp::EntryRow::from_instance(self);
        self_.suffixes.append(widget);
        self_.suffixes.show();
    }

    pub fn add_prefix<T: IsA<gtk::Widget>>(&self, widget: &T) {
        let self_ = imp::EntryRow::from_instance(self);
        self_.prefixes.append(widget);
        self_.prefixes.show();
    }

    pub fn remove<T: IsA<gtk::Widget>>(&self, child: &T) {
        let self_ = imp::EntryRow::from_instance(self);
        if let Some(parent) = child.parent() {
            if parent == self_.suffixes.get() {
                self_.suffixes.remove(child);
            } else if parent == self_.prefixes.get() {
                self_.prefixes.remove(child);
            }
        }
    }

    fn set_style(&self) {
        let self_ = imp::EntryRow::from_instance(self);

        if self.is_editable() {
            self_.title_label.remove_css_class("title");
            self_.title_label.add_css_class("subtitle");
            return;
        }

        if self.text().as_str() != "" {
            self_.title_label.remove_css_class("title");
            self_.title_label.add_css_class("subtitle");
        } else {
            self_.title_label.add_css_class("title");
            self_.title_label.remove_css_class("subtitle");
            self_.subtitle_stack.set_visible(false);
        }
    }
}
